import Login, { LoginAction } from "./components/login/login";
import Aceuil from "./components/Aceuil/aceuil";
import './App.css'
import * as React from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Contact from "./routes/contact";
//import ProtectedRoute from "./components/ProtectedRoute";

import Root from "./routes/root";


const router = createBrowserRouter([
  {
    path: "/home",
    element: <Aceuil />,
  },
  {
    path: "/login",
    element: <Login />,
    action : LoginAction
  }
]);




export default function App() {
  
  return (
    <RouterProvider router={router} />
  )
}


