import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css'
import ErrorPage from "./error-page";
import Contact from "./routes/contact";
import Root from "./routes/root";

{/*
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Contact />,
      },
     {
         path: "contacts/:contactId/edit",
        element: <EditContact />,
        loader: contactLoader,
         },
      
    ],
  },
  
]);
*/}
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
     <App  />
  </React.StrictMode>,
);
