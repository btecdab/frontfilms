import axios from 'axios';
import { Form, redirect } from "react-router-dom";
import "./style.css"

export async function LoginAction({ request }) {
    //console.log("test login");
  const formData = await request.formData();

  try {
    const response = await axios.post("http://localhost:1337/api/auth/local", {
      identifier: formData.get("identifier"),
      password: formData.get("password"),
    }, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    console.log(response);
    const data = response.data;

    if (!data.jwt) return "Invalid credentials";

    localStorage.setItem("token", data.jwt);

    return redirect("/login");
  } catch (error) {
    console.error(error);
    return "An error occurred";
  }
}

export default function Login() {
    return (
        <main className="page login">

            <div className="container">
                <div className="screen">
                    <div className="screen__content">
                        <Form method="post" className="login">
                            <div className="login__field">
                                <i className="login__icon fas fa-user"></i>
                                <input type="text" className="login__input" placeholder="User name / Email" name="identifier" />
                            </div>
                            <div className="login__field">
                                <i className="login__icon fas fa-lock"/>
                                <input name="password" type="password" className="login__input" placeholder="Password" />
                            </div>
                            <button type='submit'>Envoyer</button>
                        </Form>
                        <div className="social-login">
                            <h3>Film Time</h3>
                            <div className="social-icons">
                                <a href="#" className="social-login__icon fab fa-instagram"></a>
                                <a href="#" className="social-login__icon fab fa-facebook"></a>
                                <a href="#" className="social-login__icon fab fa-twitter"></a>
                            </div>
                        </div>
                    </div>
                    <div className="screen__background">
                        <span className="screen__background__shape screen__background__shape4"></span>
                        <span className="screen__background__shape screen__background__shape3"></span>
                        <span className="screen__background__shape screen__background__shape2"></span>
                        <span className="screen__background__shape screen__background__shape1"></span>
                    </div>
                </div>
            </div>

          
          {/* <div className="left">
            <img src="logo.png" alt="" />
          </div>
          <div className="right">
            <h1>Se connecter</h1>
            <Form method="post" className="form">
                <label htmlFor="">Username</label>
                <input type="text" name="identifier" required />
                <label htmlFor="">Mot de passe</label>
                <input type="password" name="password" required />
                <button type="submit">Se connecter</button>
            </Form>
          </div> */}
        </main>
    )
}
